# vga-rs

Low level VGA(0xB8000) library in freestanding Rust.

## How to build

```sh

cargo add vga-rs

```
## Example

```rust
#![no_std]
extern crate vga_rs;
use vga_rs::*;
extern crate cursor_rs;

fn reset_screen() {
	let mut vga_buffer = VgaBuffer::new()
	let buffer_slice = let mut vga_buffer.as_mut_slice();
	for vchar in buffer_slice.iter_mut()
	{
		let VgaChar { codepoint: _,attribute: attr,} = vchar.get_volatile();
		vchar.set_volatile(VgaChar::new(0u8, Attribute::from_u8(attr)));
	}
    cursor_rs::set_cursor(0,0);
}

```

## Contact me

- Web: <https://github.com/hwoy>
- Email: <mailto:bosskillerz@gmail.com>
- Facebook: <https://www.facebook.com/watt.duean>
